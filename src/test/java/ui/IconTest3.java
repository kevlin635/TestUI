package ui;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.octicons.OctIcon;
import de.jensd.fx.glyphs.weathericons.WeatherIcon;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Popup;
import javafx.stage.Stage;
import ldh.common.testui.component.IconPane;

/**
 * Created by ldh on 2018/4/18.
 */
public class IconTest3 extends Application{

    @Override
    public void start(Stage primaryStage) throws Exception {
        HBox hbox = new HBox();
        Popup popup = new Popup();
        IconPane iconPane = new IconPane(FontAwesomeIcon.class, MaterialDesignIcon.class, OctIcon.class, WeatherIcon.class);

        Button b1 = new Button("show");
        b1.setOnAction(e->{
            StackPane stackPane = new StackPane();
            stackPane.setPrefSize(800, 600);
            stackPane.getChildren().add(iconPane);
            stackPane.setStyle("-fx-background-color: whitesmoke");
            popup.getContent().add(stackPane);
            popup.show(primaryStage.getScene().getWindow(), 200, 200);
        });
        hbox.getChildren().add(b1);
        Scene scene = new Scene(hbox, 800, 600);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
